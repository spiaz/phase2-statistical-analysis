library (lmerTest) # Approximate df an p-values!!
library(plyr)
library(ggplot2)
library(effects)
library(car)

###### IBERDISCAP

# IMPORT DATASET
db <- read.csv("~/Documents/Experiments/Phase 2/Mean all with session.csv", na.strings="-9999")
# Force R to considers the column a categorical
db$CT <- as.factor(db$CT)# sub("$"," ISI",db$CT)
db$Session <- as.factor(db$Session)

# H is normalized between 0-1. Multiply to get between 0-100 
#db$H <- db$H * 100

intact.db <- db[db$Group=='intact',]
isci.db <- db[db$Group=='isci',]

intact.rest.db <- intact.db
isci.rest.db <- isci.db
# Discard elements after intervention (1.3, 2.1)
intact.rest.db <- intact.db[intact.db$Session!='1.3' ,]
intact.rest.db <- intact.rest.db[intact.rest.db$Session!='2.2' ,]
isci.rest.db <- isci.db[isci.db$Session!='1.3' ,]
isci.rest.db <- isci.rest.db[isci.rest.db$Session!='2.2' ,]

# Substitute variables 
intact.rest.db$Intervention <- sub("Control", "Rest", intact.rest.db$Intervention)
#intact.rest.db$Session <- sub("Before", " Pre", intact.rest.db$Session)
#isci.rest.db$Session <- sub("Before", " Pre", isci.rest.db$Session)
isci.rest.db$Intervention <- sub("Control", "Rest", isci.rest.db$Intervention)

cdata <- ddply(intact.rest.db, c("Session", "CT"), summarise, N=sum(!is.na(H)), mean = mean(H, na.rm=T), sd = sd(H, na.rm=T), se = sd/sqrt(N))
ggplot(cdata, aes(y=mean, x=CT, fill=Session)) + geom_bar(position=position_dodge(), stat="identity") + geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9) ) + ylab("Mean H") + coord_cartesian(ylim=c(0,130))

cdata <- ddply(isci.rest.db, c("Session", "CT"), summarise, N=sum(!is.na(H)), mean = mean(H, na.rm=T), sd = sd(H, na.rm=T), se = sd/sqrt(N))
ggplot(cdata, aes(y=mean, x=CT, fill=Session)) + geom_bar(position=position_dodge(), stat="identity") + geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9) ) + ylab("Mean H") + coord_cartesian(ylim=c(0,130))

