To reproduce the analysis:

1. Extract the data in the folder.
2. Run "Plot EMG.ipynb"
3. Run "Analyze raw CT.ipynb"
4. Run "Analyze all subjs using mean.ipynb"
5. Run appropriate R file

